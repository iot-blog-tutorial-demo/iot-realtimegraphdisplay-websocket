package com.godreg.websocket;

import java.io.IOException;
//import java.util.Calendar;
//import java.util.Date;
import java.util.List;

import java.util.concurrent.CopyOnWriteArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
//import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.CrossOrigin;
//import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.handler.TextWebSocketHandler;
//import org.springframework.web.socket.server.standard.TomcatRequestUpgradeStrategy;
//import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;


@Configuration
@EnableWebSocket
@EnableWebSocketMessageBroker
@EnableScheduling
@CrossOrigin
public class SchedularConfig implements WebSocketConfigurer {



	@Override
	@CrossOrigin
	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
		registry.addHandler(new QuestionHandler(), "/kcl-websocket/{sessionName}").setAllowedOrigins("*").withSockJS();

	}

	class QuestionHandler extends TextWebSocketHandler {

		private List<WebSocketSession> sessions = new CopyOnWriteArrayList<>();

		@Override
		public void afterConnectionEstablished(WebSocketSession session) throws Exception {
			sessions.add(session);
		}

		@Override
		protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
			
				System.out.println(session.getUri());
				try {
					System.out.println("Client : "+ message.getPayload());
					Jedis jedis = new Jedis("localhost");
//					Scanner scanner = new Scanner(System.in);
					String channel ="godreg-channel";
//					Calendar cal = Calendar.getInstance();
//					Date date=cal.getTime();
//					System.out.println(date+" Message:"+message);

					System.out.println("Starting subscriber for channel " + channel);

						jedis.subscribe(new JedisPubSub() {
							@Autowired
//							private SimpMessagingTemplate template;
							@Override
							public void onMessage(String channel, String rmessage) {
								super.onMessage(channel, rmessage);

								System.out.println("Received message:" + rmessage);
								try {
									synchronized (session) {
										session.sendMessage(new TextMessage(rmessage));	
									}

								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}

							@Override
							public void onSubscribe(String channel, int subscribedChannels) {

							}

							@Override
							public void onUnsubscribe(String channel, int subscribedChannels) {
							}

							@Override
							public void onPMessage(String pattern, String channel, String message) {
							}

							@Override
							public void onPUnsubscribe(String pattern, int subscribedChannels) {
							}

							@Override
							public void onPSubscribe(String pattern, int subscribedChannels) {
							}

						}, channel);

					
						jedis.close();




					//					s.sendMessage(rmessage);
				} catch (Exception e) {
					e.printStackTrace();
				}
			
		}

	}
}
