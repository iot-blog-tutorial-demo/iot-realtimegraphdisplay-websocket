package com.godreg.websocket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GodregWebsocketApplication {

	public static void main(String[] args) {
		SpringApplication.run(GodregWebsocketApplication.class, args);
	}

}
